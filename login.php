<?php
	require_once("action/LoginAction.php");

	$action = new LoginAction();
	$action->execute();

	require_once("partial/header.php");
?>

	<h1>Log in</h1>

	<div class="login-form-frame">
		<form action="login.php" method="post" class="col-lg-10">
			<?php 
				if ($action->wrongLogin) {
					?>
					<div class="alert alert-danger"><strong>Error : </strong>Information not valid</div>
					<?php
				}
			?>

			<div class="form-group ">
				<label for="username">Username:</label>
				<input type="username" name="username" class="form-control" id="username">
			</div>
			<div class="form-group">
				<label for="pwd">Password:</label>
				<input type="password" name="pwd" class="form-control" id="pwd">
			</div>
			<div class="checkbox">
				<label><input name="remember" type="checkbox"> Remember me</label>
			</div>
			<button type="submit" class="btn btn-default">Submit</button>
		</form> 
	</div>
	
<?php
	require_once("partial/footer.php");
