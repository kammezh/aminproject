<?php
	require_once("action/ViewDocumentAction.php");

	$action = new ViewDocumentAction();
	$action->execute();

	require_once("partial/header.php");
?>

<div class="text-center">

    <h1><?= $action->doc["DOC_NAME"] ?></h1>
    <div>&nbsp;</div>
    <img src="<?= $action->doc["IMAGE_PATH"] ?>" alt="doc_image" style="width:200px;">
    <div>&nbsp;</div>
    <div>&nbsp;</div>

    <?php
	// Logged in with user rights
	if ($_SESSION["visibility"] >= CommonAction::$VISIBILITY_MEMBER) {

        if ($action->doc["INFO_USER"] == "Available") {
          ?>
          <div>Returned at <?= $action->doc["DATE_MODIFIED"] ?></div>
          <div>&nbsp;</div>
          <a href="take.php?id=<?= $action->doc["ID"] ?>" type="button" class="btn btn-info"> Take </a>
          <?php
        } else {
          ?>
          <div>Taken at <?= $action->doc["DATE_MODIFIED"] ?></div>
          <div>&nbsp;</div>
          <div>By <?= $action->doc["INFO_USER"] ?></div>
          <?php
        }
    ?>

    <?php
    }
	?>
    <div>&nbsp;</div>
    <?php
	// Logged in with user and admin rights
	if ($_SESSION["visibility"] >= CommonAction::$VISIBILITY_ADMIN) {
	?>

    <a href="delete.php?id=<?= $action->doc["ID"] ?>" type="button" class="btn btn-danger"> Delete </a>

    <?php
    }
	?>
    <div>&nbsp;</div>
    <a href="alldocuments.php" type="button" class="btn btn-default"> Go back to list </a>

</div>