<?php
	require_once("action/AllUsersAction.php");

	$action = new AllUsersAction();
	$action->execute();

	require_once("partial/header.php");
?>

<div class="text-center">
<table class="table table-striped">
    <thead>
      <tr>
	  	<th class="text-center">Username</th>
        <th class="text-center">Email</th>
        <th class="text-center">Level</th>
      </tr>
    </thead>
    <tbody>
    <?php
    foreach ($action->allUser as $user) {
        
        ?>
    <tr>
        <td class="text-center">  <?= $user["USERNAME"] ?> </a> </td>
        <td class="text-center"> <?= $user["EMAIL"] ?> </td>
        <td class="text-center"> <?= $user["ACCOUNT_TYPE_NAME"] ?> </td>
    </tr>
	<tr>
		<td class="text-center"> Account options : </td>
		<td class="text-center"> <a href="changelevel.php?id=<?= $user["ID"] ?>&amp;level=1" type="button" class="btn btn-warning"> Disable </a> 
		<a href="changelevel.php?id=<?= $user["ID"] ?>&level=2" type="button" class="btn btn-primary"> Enable </a> </td>
        <td class="text-center"> <a href="changelevel.php?id=<?= $user["ID"] ?>&amp;level=3" type="button" class="btn btn-success"> Make Admin </a> </td>
	</tr>

      <?php
    }
      ?>
    </tbody>
  </table>
</div>