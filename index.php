<?php
	require_once("action/IndexAction.php");

	$action = new IndexAction();
	$action->execute();

	require_once("partial/header.php");
?>

<div class="text-center">

	<h1>Welcome
	<?php
	if (!empty($_SESSION["user"])) {
	echo " ".$_SESSION["user"]["FIRST_NAME"]." ".$_SESSION["user"]["LAST_NAME"];
	}
	else {
		?>
			to DBOX
		<?php
	}
	?>
	</h1>
		<p></p>
		<?php
		//PUBLIC
		if ($_SESSION["visibility"] == CommonAction::$VISIBILITY_PUBLIC) {
			?>
			<img src="../images/icon.png" alt="" style="margin : 30% 0%;">
			<?php
		}
		//UNAPPROVED
	if ($_SESSION["visibility"] == CommonAction::$VISIBILITY_UNAPPROVED) {
		?>
		<p>Please contact your administrator to authorize your account</p>
		<?php
	}
	//MEMBER & ADMIN
	if ($_SESSION["visibility"] >= CommonAction::$VISIBILITY_MEMBER) {
	?>
	<!-- href="scan.php" -->
	<!-- TO be implemented in the future if necessary -->
	<!-- <a style="display:block;" href="scan.php" type="button" class="btn btn-default"><span class="glyphicon glyphicon-qrcode"></span> Scan document</a> -->
	<a style="display:block;" href="alldocuments.php" type="button" class="btn btn-default"><span class="glyphicon glyphicon-book"></span> All documents</a>
	<?php
	}
	?>
	<?php
	//ADMIN
	if ($_SESSION["visibility"] >= CommonAction::$VISIBILITY_ADMIN) {
		?>
		<a style="display:block;" href="adddocument.php" type="button" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> Add document</a>
		<a style="display:block;" href="allusers.php" type="button" class="btn btn-default"><span class="glyphicon glyphicon-tent"></span> Manage users</a>
		<a style="display:block;" href="documenttype.php" type="button" class="btn btn-default"><span class=" 	glyphicon glyphicon-file"></span> Manage types</a>
	<?php	
	}
	?>

</div>

<?php
if (!empty($_SESSION["user"])){
	?>

<div class="text-center">
<h2>Taken document</h2>
<table class="table table-striped">
    <thead>
      <tr>
        <th class="text-center">Name</th>
        <th class="text-center">Author/Owner</th>
        <th class="text-center">Type</th>
        <th class="text-center">Return</th>
        <!-- <th class="text-center">Taken on</th> -->
        <!-- <th class="text-center">Popularity</th> -->
      </tr>
    </thead>
    <tbody>
    <?php
    foreach ($action->allDoc as $doc) {
        
        ?>
    <tr>
        <td class="text-center"> <?= $doc["DOC_NAME"] ?> </td>
        <td class="text-center"> <?= $doc["DOC_AUTHOR"] ?> </td>
        <td class="text-center"> <?= $doc["DOC_TYPE"] ?> </td>
        <td class="text-center"> <a href="bringback.php?id=<?= $doc["ID"] ?>" type="button" class="btn btn-info"> Return </a> </td>
        <!-- <td class="text-center"> <?php //echo $doc["DATE_MODIFIED"] ?> </td> -->
        <!-- <td class="text-center"> <?php //echo $doc["POPULARITY"] ?> </td> -->
        
        
    </tr>

      <?php
    }
      ?>
    </tbody>
  </table>
</div>

	<?php
}
	require_once("partial/footer.php");
	//Laferrari
