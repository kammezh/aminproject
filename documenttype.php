<?php
	require_once("action/DocumentTypeAction.php");

	$action = new DocumentTypeAction();
	$action->execute();

	require_once("partial/header.php");
?>

    <h1>Type</h1>

    <!-- Display of operations results -->
	<?php
		if ($action->success) {
			?>
			<div class="alert alert-success col-lg-10"><strong>Success! </strong>
			<?php 
			if ($action->success_add) { ?> <div>Type added</div> <?php }
            if ($action->success_modify) { ?> <div>Type name modified</div> <?php }
            if ($action->success_delete) { ?> <div>Type deleted</div> <?php } 
			?>
			</div>
			<?php
		}
		if ($action->error) {
			?>
			<div class="alert alert-danger col-lg-10"><strong>Error! </strong>
			<?php 
			if ($action->error_add) { ?> <div>Type name is too long or empty</div> <?php }
			if ($action->error_modify) { ?> <div>Type name is too long or empty</div> <?php }
			if ($action->error_delete) { ?> <div>Type is used in one or more products</div> <?php }
			?>
			</div>
			<?php
		}
	?>

    <!-- Add type -->
    <form action="documenttype.php" method="post" class="col-lg-10">
        <h2>Add</h2>
        <div class="form-group">
			<label for="add_type">New type name : </label>
			<input type="text" name="add_type" class="form-control" placeholder="Maximum 20 characters">
		</div>
        <input type="hidden" name="action" value="add">
        <button type="submit" class="btn btn-default">Add</button>
    </form>
    <!-- Modify type -->
    <form action="documenttype.php" method="post" class="col-lg-10">
        <h2>Edit</h2>
        <div class="form-group">
			<label for="modify_type_new">New type name : </label>
			<input type="text" name="modify_type_new" class="form-control" placeholder="Maximum 20 characters">
		</div>
        <div class="form-group">
            <label for="modify_type_old">Select type to modify : </label>
            <select name="modify_type_old" class="form-control">
            <?php
            $first_item = true;
            foreach ($action->list_type as $type) {
                ?>
                <option value="<?= $type["ID"] ?>" <?php if($first_item) { echo "selected='selected'";$first_item = false; } ?> > <?= $type["TYPE_NAME"] ?> </option>
                <?php
            }
            ?>
            </select>
        </div>
        <input type="hidden" name="action" value="modify">
        <button type="submit" class="btn btn-default">Modify</button>
    </form>
    <!-- Delete type -->
    <form action="documenttype.php" method="post" class="col-lg-10">
        <h2>Delete</h2>
        <div class="form-group">
            <label for="sel1">Select type to remove :</label>
            <select name="remove_type" class="form-control">
            <?php
            $first_item = true;
            foreach ($action->list_type as $type) {
                ?>
                <option value="<?= $type["ID"] ?>" <?php if($first_item) { echo "selected='selected'";$first_item = false; } ?>> <?= $type["TYPE_NAME"] ?> </option>
                <?php
            }
            ?>
            </select>
        </div>
        <input type="hidden" name="action" value="delete">
        <button type="submit" class="btn btn-default">Delete</button>
    </form>

<?php
	require_once("partial/footer.php");