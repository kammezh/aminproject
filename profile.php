<?php
	require_once("action/ProfileAction.php");

	$action = new ProfileAction();
	$action->execute();

	require_once("partial/header.php");
?>
	<h1>Profile</h1>
	
	<p>View or modify your profile :</p>

	<!-- Display of operations results -->
	<?php
		if ($action->success) {
			?>
			<div class="alert alert-success col-lg-10"><strong>Success! </strong>
			<?php 
			if ($action->success_profile) { ?> <div>Profile updated</div> <?php }
			if ($action->success_password) { ?> <div>Password updated</div> <?php } 
			?>
			</div>
			<?php
		}
		if ($action->error) {
			?>
			<div class="alert alert-danger col-lg-10"><strong>Error! </strong>
			<?php 
			if ($action->error_first_name) { ?> <div>First name is too long or empty</div> <?php }
			if ($action->error_last_name) { ?> <div>Last name is too long or empty</div> <?php }
			if ($action->error_email) { ?> <div>Email is too long or empty</div> <?php }
			if ($action->exist_email) { ?> <div>Email is already used by an account</div> <?php }
			if ($action->error_username) { ?> <div>Username is too long or empty</div> <?php }
			if ($action->exist_username) { ?> <div>Username is already taken</div> <?php }
			if ($action->error_password) { ?> <div>Password is either too short or too long and must be filled twice</div> <?php }
			?>
			</div>
			<?php
		}
	?>

	<form action="profile.php" method="post" class="col-lg-10">
		<div class="form-group">
			<label for="first_name">First name : </label>
			<input type="text" name="first_name" class="form-control" placeholder="Maximum 50 characters" value="<?= $_SESSION["user"]["FIRST_NAME"] ?>">
		</div>
		<div class="form-group">
			<label for="last_name">Last name : </label>
			<input type="text" name="last_name" class="form-control" placeholder="Maximum 50 characters" value="<?= $_SESSION["user"]["LAST_NAME"] ?>">
		</div>
		<div class="form-group">
			<label for="email">Email : </label>
			<input type="email" name="email" class="form-control" placeholder="Enter a valid email address" value="<?= $_SESSION["user"]["EMAIL"] ?>">
		</div>
		<div class="form-group">
			<label for="username">Username : </label>
			<input type="text" name="username" class="form-control" placeholder="Maximum 20 characters" value="<?= $_SESSION["user"]["USERNAME"] ?>">
		</div>
		<p>Modify your password :</p>
		<div class="form-group">
			<label for="pwd">New Password : </label>
			<input type="password" name="pwd" class="form-control" placeholder="Between 8 and 20 characters">
		</div>
		<div class="form-group">
			<label for="pwdverify">Retype password : </label>
			<input type="password" name="pwdverify" class="form-control" placeholder="Confirm password">
		</div>
		<button type="submit" class="btn btn-default">Submit</button>
	</form>	
	
<?php
	require_once("partial/footer.php");
