<?php
	require_once("action/AddDocumentAction.php");

	$action = new AddDocumentAction();
	$action->execute();

	require_once("partial/header.php");
?>
	<script src="js/filename.js"></script>

	<h1>Document</h1>
	
	<p>View or modify a document :</p>

	<!-- Display of operations results -->
	<?php
		if ($action->success) {
			?>
			<div class="alert alert-success col-lg-10"><strong>Success! </strong>Document added</div>
			<?php
		}
		if ($action->error) {
			?>
			<div class="alert alert-danger col-lg-10"><strong>Error! </strong>
			<?php
			if ($action->error_document_name) { ?> <div>Document name is too long or empty</div> <?php }
			if ($action->error_document_author) { ?> <div>Document author is too long or empty</div> <?php }
			if ($action->error_description) { ?> <div>Description is too long</div> <?php }
			if ($action->error_type) { ?> <div>Document type not selected</div> <?php }
			if ($action->error_qr_code) { ?> <div>QR Code not accepted</div> <?php }
			if ($action->error_image) { ?> <div>Problem uploading image</div> <?php }
			?>
			</div>
			<?php
		}
	?>

	<form action="adddocument.php" method="post" enctype="multipart/form-data" class="col-lg-10">
		<div class="form-group">
			<label for="document_name">Document name : </label>
			<input type="text" name="document_name" class="form-control" placeholder="Maximum 100 characters" required>
		</div>
		<div class="form-group">
		<label for="document_author">Document author : </label>
			<input type="text" name="document_author" class="form-control" placeholder="Maximum 100 characters" required>
		</div>
		<div class="form-group">
			<label for="description">Description : </label>
			<textarea type="text" rows="5" name="description" class="form-control" placeholder="Maximum 500 characters"></textarea>
		</div>
		<div class="form-group">
            <label for="document_type">Document type :</label>
            <select name="document_type" class="form-control">
            <?php
            $first_item = true;
            foreach ($action->list_type as $type) {
                ?>
                <option value="<?= $type["ID"] ?>" <?php if($first_item) { echo "selected='selected'";$first_item = false; } ?>> <?= $type["TYPE_NAME"] ?> </option>
                <?php
            }
            ?>
            </select>
        </div>
		<div class="form-group">
			<label for="document_qr_code">Document QR Code : </label>
			<input type="text" name="document_qr_code" class="form-control" placeholder="Maximum 100 characters">
		</div>
		<div class="form-group">
			<label for="document_image">Image file : </label>
			<div class="input-group">
				<input type="text" name="document_image" class="form-control" id="image-path" placeholer="Image path" disable>
				<span class="input-group-btn">
					<label class="btn btn-default">
						Browse	<input type="file" name="document_image" id="image-selector" style="display:none">
					</label>
				</span>
			</div>
		</div>
		<div class="text-center">
			<img id="output_image" class="img-rounded" style="height:230px;"/>
		</div>
		<button type="submit" class="btn btn-default">Submit</button>
	</form>
	
<?php
	require_once("partial/footer.php");
