<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/DocumentTypeDAO.php");
	require_once("action/dao/DocumentDAO.php");
	require_once("action/dao/UserDAO.php");

	class AllDocumentsAction extends CommonAction {

		public $allDoc = array();
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_MEMBER);			
		}

		protected function executeAction() {

			$this->allDoc = DocumentDAO::readAll();
			$typeChart = DocumentTypeDAO::readAll();
			$userChart = UserDAO::readAll();
			$tempAllDoc =array();

			foreach ($this->allDoc as $doc) {

				foreach ($typeChart as $type) {

					if ( $temp = array_search($doc["ID_TYPE"],$type) ) {

						$doc["DOC_TYPE"] = $type["TYPE_NAME"];
					}
				}

				if ($doc["USER_ID"]) {

					foreach ($userChart as $user) {

						if ($doc["USER_ID"] === $user["ID"]) {					
							$doc["INFO_USER"] = $user["FIRST_NAME"] . " " . $user["LAST_NAME"];
						}
					}
				}
				else {
					$doc["INFO_USER"] = "Available";
				}

				array_push($tempAllDoc,$doc);
			}

			$this->allDoc = $tempAllDoc;

		}
	}
