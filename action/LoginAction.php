<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/UserDAO.php");

	class LoginAction extends CommonAction {
		public $wrongLogin = false;
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {

			if (empty($_SESSION["user"])){

				if (!empty($_POST["username"])) {
					
					$user = UserDAO::authenticate($_POST["username"], $_POST["pwd"]);

					if ($user != null) {
						
						$_SESSION["visibility"] = (int) $user["ACCOUNT_TYPE"];
						$_SESSION["user"] = $user;
							
						header("location:index.php");
						exit;
					}
					else {
						$this->wrongLogin = true;
					}
				}
			}
			else {
				header("location:index.php");
				exit;
			}
			

			
		}
	}
