<?php
    require_once("action/CommonAction.php");
    require_once("action/dao/DocumentDAO.php");

	class DeleteAction extends CommonAction {
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_ADMIN);
		}

		protected function executeAction() {

        if (empty($_SESSION)) {
            header("location:login.php");
		    exit;
        }

        DocumentDAO::remove($_GET["id"]);
        
        header("location:alldocuments.php");
		exit;
		
		}
	}
