<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/UserDAO.php");

	class SignUpAction extends CommonAction {
        public $success = false;

        public $error = false;
        public $error_first_name = false;
        public $error_last_name = false;
        public $error_email = false;
        public $error_username = false;
        public $error_password = false;

        public $exist_username = false;
        public $exist_email = false;
        
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {

            if ( !empty( $_POST["username"] ) ) {
                $user = null;

                // First name
                if (!empty($_POST["first_name"])
                && strlen($_POST["first_name"]) <= 50 ) {

                    $user["FIRST_NAME"] = $_POST["first_name"];
                }
                else {
                    $this->error = true;
                    $this->error_first_name = true;
                }
                
                //Last name
                if (!empty($_POST["last_name"])
                && strlen($_POST["last_name"]) <= 50 ) {

                    $user["LAST_NAME"] = $_POST["last_name"];
                }
                else {
                    $this->error = true;
                    $this->error_last_name = true;
                }

                //Email
                if (!empty($_POST["email"])
                && strlen($_POST["email"]) <= 100
                && filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
                    
                    $this->exist_email = UserDAO::verifyEmailPresence($_POST["email"]);
                    // I don't want to access the DB if the username is not valid
                    // this is why this condition seems repetitive
                    if (!$this->exist_email) {
                        $user["EMAIL"] = $_POST["email"];
                    }
                    else {
                        $this->error = true;
                    }   
                }
                else {
                    $this->error = true;
                    $this->error_email = true;
                }

                //Username
                if (!empty($_POST["username"])
                && strlen($_POST["username"]) <= 20 ) {

                    $this->exist_username = UserDAO::verifyUsernamePresence($_POST["username"]);
                    // I don't want to access the DB if the username is not valid
                    // this is why this condition seems repetitive
                    if (!$this->exist_username) {
                        $user["USERNAME"] = $_POST["username"];
                    }
                    else {
                        $this->error = true;
                    }
                }
                else {
                    $this->error = true;
                    $this->error_username = true;
                }

                //Password
                if (!empty($_POST["pwd"])
                && strlen($_POST["pwd"]) >= 8 
                && strlen($_POST["pwd"]) <= 20
                && $_POST["pwd"] == $_POST["pwdverify"]) {

                    $user["HASH_PASSWORD"] = $_POST["pwd"];
                }
                else {
                    $this->error = true;
                    $this->error_password = true;
                }

                //Insert in database
                if (!$this->error) {

                        /* Temporary for testing purpose */
                        $user["IMAGE_ID"] = "";
                        /* Temporary for testing purpose */
                        UserDAO::create($user);
                        $this->success = true;
                    }
            }
        }
    }