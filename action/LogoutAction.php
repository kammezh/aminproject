<?php
	require_once("action/CommonAction.php");

	class LogoutAction extends CommonAction {
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_UNAPPROVED);
		}

		protected function executeAction() {

            session_unset();
		session_destroy();
            header("location:index.php");
		exit;
		
		}
	}
