<?php
    require_once("action/CommonAction.php");
    require_once("action/dao/UserDAO.php");

	class ChangeLevelAction extends CommonAction {
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_ADMIN);
		}

		protected function executeAction() {

            UserDAO::changeAccountType($_GET["id"],$_GET["level"]);
        
            header("location:allusers.php");
		    exit;
		
		}
	}
