<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/DocumentDAO.php");
	require_once("action/dao/DocumentTypeDAO.php");

	class AddDocumentAction extends CommonAction {

		public $success = false;

		public $error = false;
		public $error_document_name = false;
		public $error_document_author = false;
		public $error_description = false;
		public $error_type = false;
		public $error_qr_code = false;
		public $error_image = false;

		public $list_type;
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_ADMIN);
        }

        protected function executeAction() {

			$this->list_type = DocumentTypeDAO::readAll();

			if (!empty($_POST["document_name"])) {

				$document = null;
				$image = null;

				//Product name
				if (!empty($_POST["document_name"])
				&& strlen($_POST["document_name"]) <= 100) {
					$document["DOC_NAME"] = $_POST["document_name"];
				}
				else {
					$this->error = true;
					$this->error_document_name = true;
				}

				//Product author
				if (!empty($_POST["document_author"])
				&& strlen($_POST["document_author"]) <= 100) {
					$document["DOC_AUTHOR"] = $_POST["document_author"];
				}
				else {
					$this->error = true;
					$this->error_document_author = true;
				}

				//Product description
				if (!empty($_POST["description"]) ) {

					if (strlen($_POST["description"]) <= 1500) {
						$document["DESCRIPTION"] = $_POST["description"];
					}
					else {
						$this->error = true;
						$this->error_description = true;
					}
				}

				//Product QR Code
				if (strlen($_POST["document_qr_code"]) <= 100) {
					$document["QR_CODE"] = $_POST["document_qr_code"];
				}
				else {
					$this->error = true;
					$this->error_qr_code = true;
				}

				//Product type
				if (!empty($_POST["document_type"]) ) {
					$document["ID_TYPE"] = $_POST["document_type"];
				}
				else {
					$this->error = true;
					$this->error_type = true;
				}
				
				// Image File
				if (!empty($_FILES["document_image"]["name"])
				&& $_FILES["document_image"]["error"] == 0) {

					// Verify image file (not corrupt and picture format)
					$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
					$detectedType = exif_imagetype($_FILES['document_image']['tmp_name']);

					if (in_array($detectedType, $allowedTypes)
					&& $_FILES["document_image"]["size"] > 0
					&& strlen($_FILES["document_image"]["name"]) <= 400) {

						//modify name with a random number
						$newfilename = round(microtime(true)) . '_' . $_FILES["document_image"]["name"];
						//upload image here
						$document["IMAGE_PATH"] = "documentimage/" . $newfilename;
						move_uploaded_file($_FILES["document_image"]["tmp_name"], $document["IMAGE_PATH"]);

					}
					else {
						$this->error = true;
						$this->error_image = true;
					}
				}
				else {
					$this->error = true;
					$this->error_image = true;
				}

				if (!$this->error) {
                    DocumentDAO::create($document);
                    $this->success = true;
                }

			}
		}
    }