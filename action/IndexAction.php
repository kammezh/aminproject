<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/DocumentDAO.php");
	require_once("action/dao/DocumentTypeDAO.php");

	class IndexAction extends CommonAction {

		public $allDoc = array();
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {

			if (!empty($_SESSION["user"])) {
			$this->allDoc = DocumentDAO::readByUserId($_SESSION["user"]["ID"]);
			$typeChart = DocumentTypeDAO::readAll();
			$tempAllDoc =array();

			foreach ($this->allDoc as $doc) {

				foreach ($typeChart as $type) {

					if ( $temp = array_search($doc["ID_TYPE"],$type) ) {

						$doc["DOC_TYPE"] = $type["TYPE_NAME"];
					}
				}

				array_push($tempAllDoc,$doc);
			}

			$this->allDoc = $tempAllDoc;

			}
		}
	}	
