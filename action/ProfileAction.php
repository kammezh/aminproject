<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/UserDAO.php");

	class ProfileAction extends CommonAction {
		public $success = false;
		public $success_profile = false;
		public $success_password = false;

		public $error = false;
		public $error_first_name = false;
        public $error_last_name = false;
        public $error_email = false;
        public $error_username = false;
		public $error_password = false;
		
		public $exist_username = false;
		public $exist_email = false;
		
		public $change = false;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_UNAPPROVED);
		}

		protected function executeAction() {
	
			if ($_SERVER['REQUEST_METHOD'] === 'POST') {
				// First name
				if ($_SESSION["user"]["FIRST_NAME"] != $_POST["first_name"]) {

					if (!empty($_POST["first_name"])
					&& strlen($_POST["first_name"]) <= 50) {
		
						$_SESSION["user"]["FIRST_NAME"] = $_POST["first_name"];
						$this->change = true;
					}
					else {
						$this->error = true;
						$this->error_first_name = true;
					}
				}
				
				//Last name
				if ($_SESSION["user"]["LAST_NAME"] != $_POST["last_name"]) {

					if (!empty($_POST["last_name"])
					&& strlen($_POST["last_name"]) <= 50) {
		
						$_SESSION["user"]["LAST_NAME"] = $_POST["last_name"];
						$this->change = true;
					}
					else {
						$this->error = true;
						$this->error_last_name = true;
					}
				}

				//Email
				if ($_SESSION["user"]["EMAIL"] != $_POST["email"]) {

					if (!empty($_POST["email"])
					&& strlen($_POST["email"]) <= 100
					&& filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) ) {
		
						$this->exist_email = UserDAO::verifyEmailPresence($_POST["email"]);
						// I don't want to access the DB if the username is not valid
						// this is why this condition seems repetitive
						if (!$this->exist_email) {
							$_SESSION["user"]["EMAIL"] = $_POST["email"];
							$this->change = true;
						}
						else {
							$this->error = true;
						}   
					}
					else {
						$this->error = true;
						$this->error_email = true;
					}
				}

				//Username
				if ($_SESSION["user"]["USERNAME"] != $_POST["username"]) {

					if (!empty($_POST["username"]
					&& strlen($_POST["username"]) <= 20)) {
		
						$this->exist_username = UserDAO::verifyUsernamePresence($_POST["username"]);
						// I don't want to access the DB if the username is not valid
						// this is why this condition seems repetitive
						if (!$this->exist_username) {
							$_SESSION["user"]["USERNAME"] = $_POST["username"];
							$this->change = true;
						}
						else {
							$this->error = true;
						}
					}
					else {
						$this->error = true;
						$this->error_username = true;
					}
				}
				
				//Update in DB
				if (!$this->error && $this->change) {

					UserDAO::update($_SESSION["user"]);
					$this->success = true;
					$this->success_profile = true;
				}

				//Password
				if (!empty($_POST["pwd"])) {

					if (!empty($_POST["pwdverify"])
					&& strlen($_POST["pwd"]) >= 8 
					&& strlen($_POST["pwd"]) <= 20
					&& $_POST["pwd"] == $_POST["pwdverify"]) {
						
						//Update in DB for password only
						UserDAO::updatePassword($_SESSION["user"],$_POST["pwd"]);
						$this->success = true;
						$this->success_password = true;
					}
					else {
						$this->error = true;
						$this->error_password = true;
					}
				}
			}
		}
	}
