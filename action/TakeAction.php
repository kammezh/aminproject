<?php
    require_once("action/CommonAction.php");
    require_once("action/dao/DocumentDAO.php");

	class TakeAction extends CommonAction {
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {

        if (empty($_SESSION)) {
            header("location:login.php");
		    exit;
        }

        DocumentDAO::take($_SESSION["user"],$_GET["id"]);
        
        header("location:index.php");
		exit;
		
		}
	}
