<?php
    require_once("action/CommonAction.php");
    require_once("action/dao/DocumentDAO.php");

	class BringBackAction extends CommonAction {
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {

        if (!empty($_SESSION)) {
            DocumentDAO::bringback($_GET["id"]);
        }

        header("location:index.php");
		exit;
		
		}
	}
