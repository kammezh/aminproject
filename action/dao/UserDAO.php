<?php
	require_once("action/dao/Connection.php");

	class UserDAO {

		public static function create($user) {
			$connection = Connection::getConnection();

			$statement = $connection->prepare("INSERT INTO AP_USER 
												(username , hash_password , first_name , last_name , email , image_id)
												VALUES ( ? , ? , ? , ? , ? , ? )");
			$statement->bindParam(1, $user["USERNAME"]);
			$user["HASH_PASSWORD"] = password_hash(($user["HASH_PASSWORD"]), PASSWORD_BCRYPT);
			$statement->bindParam(2, $user["HASH_PASSWORD"]);
			$statement->bindParam(3, $user["FIRST_NAME"]);
            $statement->bindParam(4, $user["LAST_NAME"]);
			$statement->bindParam(5, $user["EMAIL"]);
			$statement->bindParam(6, $user["IMAGE_ID"]);	
            
			$statement->execute();

			$id = $connection->lastInsertId();
            return $id;
		}

		public static function read($id) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("SELECT * FROM AP_USER WHERE ID = ?");
			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            
            $user = $statement->fetch();

            return $user;
        }

		public static function changeAccountType($id,$level) {
			$connection = Connection::getConnection();

			$statement = $connection->prepare("UPDATE AP_USER SET account_type = ? WHERE id = ?");
			$statement->bindParam(1, $level);
			$statement->bindParam(2, $id);
			$statement->execute();
		}



		public static function updatePassword($user , $password) {
			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("UPDATE AP_USER
												SET HASH_PASSWORD = ?
												WHERE id = ?");
			// Hash the password before sending it
			$hash = password_hash(($password), PASSWORD_BCRYPT);
			$statement->bindParam(1, $hash);
			$statement->bindParam(2, $user["ID"]);
			$statement->execute();
		}

		public static function remove($user) {
			$connection = Connection::getConnection();

			$statement = $connection->prepare("DELETE FROM AP_USER WHERE id = ?");
			$statement->bindParam(1,$user["ID"]);
			$statement->execute();
		}

		public static function readAll() {
			$connection = Connection::getConnection();

			$statement = $connection->prepare("SELECT * FROM AP_USER");
			$statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();

            $listUser = $statement->fetchAll();
            return $listUser;
		}

		public static function authenticate($username, $password) {
			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("SELECT * FROM AP_USER WHERE USERNAME = ?");
			$statement->bindParam(1, $username);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$user = $statement->fetch();

			if ( password_verify( $password ,$user["HASH_PASSWORD"] ) ) {
				unset($user["HASH_PASSWORD"]);
				return $user;
			}
			else {
				return null;
			}
		}

		public static function verifyEmailPresence($email) {
			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("SELECT * FROM AP_USER WHERE EMAIL = ? LIMIT 1");
			$statement->bindParam(1, $email);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$presence = $statement->fetch();

			if ($presence == null) {
				return false;
			}
			else {
				return true;
			}
		}

		public static function verifyUsernamePresence($username) {
			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("SELECT * FROM AP_USER WHERE USERNAME = ? LIMIT 1");
			$statement->bindParam(1, $username);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$presence = $statement->fetch();

			if ($presence == null) {
				return false;
			}
			else {
				return true;
			}
		}
	}