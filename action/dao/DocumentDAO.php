<?php
	require_once("action/dao/Connection.php");

	class DocumentDAO {

        public static function create($product) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("INSERT INTO AP_DOC (doc_name , doc_author , 
             description , qr_code , image_path  , id_type) VALUES ( ? , ? , ? , ? , ? , ? )");
            $statement->bindParam(1, $product["DOC_NAME"]);
            $statement->bindParam(2, $product["DOC_AUTHOR"]);
            $statement->bindParam(3, $product["DESCRIPTION"]);
            $statement->bindParam(4, $product["QR_CODE"]);
            $statement->bindParam(5, $product["IMAGE_PATH"]);
            $statement->bindParam(6, $product["ID_TYPE"]);
            
            $statement->execute();
            
            $id = $connection->lastInsertId();
            return $id;
        }

        public static function read($id) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("SELECT * FROM AP_DOC WHERE ID = ?");
			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            
            $product = $statement->fetch();

            return $product;
        }

        public static function update($product) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("UPDATE AP_DOC SET doc_name = ? ,
            doc_author = ? , description = ? , qr_code = ? , image_path = ? , 
            id_type = ? WHERE id = ?");
            $statement->bindParam(1, $product["DOC_NAME"]);
            $statement->bindParam(2, $product["DOC_AUTHOR"]);
            $statement->bindParam(3, $product["DESCRIPTION"]);
            $statement->bindParam(4, $product["QR_CODE"]);
            $statement->bindParam(5, $product["IMAGE_PATH"]);
            $statement->bindParam(6, $product["ID_TYPE"]);
            $statement->bindParam(7, $product["ID"]);
            $statement->execute();
        }

        public static function take($user , $docid) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("UPDATE AP_DOC SET user_id = ? WHERE id = ?");
            $statement->bindParam(1, $user["ID"]);
            $statement->bindParam(2, $docid);
            $statement->execute();
        }

        public static function bringback($docid) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("UPDATE AP_DOC SET user_id = NULL WHERE id = ?");
            $statement->bindParam(1, $docid);
            $statement->execute();
        }

        public static function remove($id) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("DELETE FROM AP_DOC WHERE id = ?");
			$statement->bindParam(1,$id);
			$statement->execute();
        }

        public static function readByUserId($user_id) {
            $connection = Connection::getConnection();
            
            $statement = $connection->prepare("SELECT * FROM AP_DOC WHERE user_id = ?");
            $statement->bindParam(1,$user_id);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();

            $listProduct = $statement->fetchAll();
            return $listProduct;
        }

        public static function readByType($idtype) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("SELECT * FROM AP_DOC WHERE id_type = ?");
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->bindParam(1,$idtype);   
            $statement->execute();

            $listProduct = $statement->fetchAll();
            return $listProduct;
        }

        public static function readAll() {
            $connection = Connection::getConnection();
            
            $statement = $connection->prepare("SELECT * FROM AP_DOC");
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();

            $listProduct = $statement->fetchAll();
            return $listProduct;
        }
    }