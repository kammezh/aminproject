<?php
	require_once("action/dao/Connection.php");

	class AccountTypeDAO {

        public static function create($type) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("INSERT INTO AP_ACCOUNT_TYPE ( account_type ) VALUES ( ? )");
            $statement->bindParam(1, $type);
            $statement->execute();
            
            $id = $connection->lastInsertId();
            return $id;
        }

        public static function read($id) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("SELECT * FROM AP_ACCOUNT_TYPE WHERE ID = ?");
			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            
            $type = $statement->fetch();

            return $type;
        }

        public static function update($type , $id) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("UPDATE AP_ACCOUNT_TYPE SET account_type = ? WHERE id = ?");
            $statement->bindParam(1, $type);
            $statement->bindParam(2, $id);            
            $statement->execute();
        }

        public static function remove($id) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("DELETE FROM AP_ACCOUNT_TYPE WHERE id = ?");
			$statement->bindParam(1,$id);
			$statement->execute();
        }

        public static function readAll() {
            $connection = Connection::getConnection();
            
            $statement = $connection->prepare("SELECT * FROM AP_ACCOUNT_TYPE ORDER BY ID DESC");
            $statement->execute();

            $listType = $statement->fetchAll();
            return $listType;
        }

    }