<?php
	require_once("action/dao/Connection.php");

	class DocumentTypeDAO {

        public static function create($producttype) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("INSERT INTO AP_TYPE ( type_name ) VALUES ( ? )");
            $statement->bindParam(1, $producttype);
            $statement->execute();
            
            $id = $connection->lastInsertId();
            return $id;
        }

        public static function read($id) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("SELECT * FROM AP_TYPE WHERE ID = ?");
			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            
            $producttype = $statement->fetch();

            return $producttype;
        }

        public static function update($producttype , $id) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("UPDATE AP_TYPE SET type_name = ? WHERE id = ?");
            $statement->bindParam(1, $producttype);
            $statement->bindParam(2, $id);            
            $statement->execute();
        }

        public static function remove($id) {
            $connection = Connection::getConnection();

            $statement = $connection->prepare("DELETE FROM AP_TYPE WHERE id = ?");
			$statement->bindParam(1,$id);
			$statement->execute();
        }

        public static function readAll() {
            $connection = Connection::getConnection();
            
            $statement = $connection->prepare("SELECT * FROM AP_TYPE ORDER BY TYPE_NAME");
            $statement->execute();

            $listType = $statement->fetchAll();
            return $listType;
        }

    }