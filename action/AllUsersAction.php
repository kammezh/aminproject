<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/UserDAO.php");
	require_once("action/dao/AccountTypeDAO.php");

	class AllUsersAction extends CommonAction {

		public $allUser = array();
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_ADMIN);
		}

		protected function executeAction() {

			$this->allUser = UserDAO::readAll();
			$levelList = AccountTypeDAO::readAll();
			$tempAllUser = array();

			foreach ($this->allUser as $user) {

				foreach ($levelList as $level) {

					if ( $temp = array_search($user["ACCOUNT_TYPE"],$level) ) {

						$user["ACCOUNT_TYPE_NAME"] = $level["ACCOUNT_TYPE"];
					}
				}

				array_push($tempAllUser,$user);
			}

			$this->allUser = $tempAllUser;

		}
	}
