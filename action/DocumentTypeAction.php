<?php
	require_once("action/CommonAction.php");
	require_once("action/dao/DocumentTypeDAO.php");

	class DocumentTypeAction extends CommonAction {
		public $list_type;

		public $success = false;
		public $success_add = false;
		public $success_modify = false;
		public $success_delete = false;

		public $error = false;
		public $error_add = false;
		public $error_modify = false;
		public $error_delete = false;
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_ADMIN);
			$this->list_type = array();
        }

        protected function executeAction() {

			if (!empty($_POST["action"])) {
				//Add type
				if ($_POST["action"] == "add") {

					if ( !empty($_POST["add_type"])
					&& strlen($_POST["add_type"]) <= 20 ) {

						DocumentTypeDAO::create($_POST["add_type"]);
						$this->success = true;
						$this->success_add = true;
					}
					else {
						$this->error = true;
						$this->error_add = true;
					}

				}
				//Modify type
				elseif ($_POST["action"] == "modify") {

					if (!empty($_POST["modify_type_new"])
					&& strlen($_POST["modify_type_new"]) <= 20 ) {
						
						DocumentTypeDAO::update($_POST["modify_type_new"] , $_POST["modify_type_old"]);
						$this->success = true;
						$this->success_modify = true;
					}
					else {
						$this->error = true;
						$this->error_modify = true;
					}
				}
				//Remove type
				elseif ($_POST["action"] == "delete") {

					if (strlen($_POST["remove_type"]) <= 20 ) {

						//verify that no other product is using this type

						DocumentTypeDAO::remove($_POST["remove_type"]);
						$this->success = true;
						$this->success_delete = true;
					}
					else {
						$this->error = true;
						$this->error_delete = true;
					}
				}

			}

			//get all types after operations
			$this->list_type = DocumentTypeDAO::readAll();
		}
    }