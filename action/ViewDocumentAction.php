<?php
    require_once("action/CommonAction.php");
    require_once("action/dao/DocumentDAO.php");
    require_once("action/dao/UserDAO.php");

	class ViewDocumentAction extends CommonAction {

        public $doc = array();
		
		public function __construct() {
			parent::__construct(parent::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {

            if (empty($_SESSION)) {
                header("location:login.php");
                exit;
            }

            $tempDoc = DocumentDAO::read($_GET["id"]);
            $userChart = UserDAO::readAll();

            if ($tempDoc["USER_ID"]) {

                foreach ($userChart as $user) {

                    if ($tempDoc["USER_ID"] === $user["ID"]) {					
                        $tempDoc["INFO_USER"] = $user["FIRST_NAME"] . " " . $user["LAST_NAME"];
                    }
                }
            }
            else {
                $tempDoc["INFO_USER"] = "Available";
            }

            $this->doc = $tempDoc;
		
		}
	}
