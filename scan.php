<?php
	require_once("action/ScanAction.php");

	$action = new ScanAction();
	$action->execute();

	require_once("partial/header.php");

?>

<script src="js/scan.js"></script>

<div class="text-center">
    <form enctype="multipart/form-data">
        <!-- MAX_FILE_SIZE (maximum file size in bytes) must precede the file input field used to upload the QR code image -->
        <input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
        <!-- The "name" of the input field has to be "file" as it is the name of the POST parameter -->
        Capture QR code to borrow: <input id="file" class="btn btn-primary" style="width:100%;" name="file" type="file" />
        <div>
            &nbsp;
        </div>
        <input id="scan" class="btn btn-primary" type="submit" value="Read QR code" />
    </form>
</div>