<?php
	require_once("action/AllDocumentsAction.php");

	$action = new AllDocumentsAction();
	$action->execute();

	require_once("partial/header.php");
?>

<div class="text-center">
<table class="table table-striped">
    <thead>
      <tr>
        <th class="text-center">Name</th>
        <th class="text-center">Author/Owner</th>
        <th class="text-center">Type</th>
        <!-- <th class="text-center">Date taken/Brought back</th> -->
        <!-- <th class="text-center">Popularity</th> -->
        <th class="text-center">Details</th>
      </tr>
    </thead>
    <tbody>
    <?php
    foreach ($action->allDoc as $doc) {
        
        ?>
    <tr>
        <td class="text-center">  <?= $doc["DOC_NAME"] ?> </a> </td>
        <td class="text-center"> <?= $doc["DOC_AUTHOR"] ?> </td>
        <td class="text-center"> <?= $doc["DOC_TYPE"] ?> </td>
        <!-- <td class="text-center"> <?php //echo $doc["DATE_MODIFIED"] ?> </td> -->
        <!-- <td class="text-center"> <?php //echo $doc["POPULARITY"] ?> </td> -->
        <td class="text-center"> <a href="viewdocument.php?id=<?= $doc["ID"] ?>" type="button" class="btn btn-info"> View </a> </td>
        
    </tr>

      <?php
    }
      ?>
    </tbody>
  </table>
</div>