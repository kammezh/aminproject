let reader = new FileReader();

window.onload = function() {

    document.getElementById("image-selector").onchange = function(event) {
        document.getElementById("image-path").value = document.getElementById("image-selector").files[0].name;

        reader.onload = function()
        {
         var output = document.getElementById('output_image');
         output.src = reader.result;
        }
        
        reader.readAsDataURL(event.target.files[0]);
    }
    
}