<!DOCTYPE html>
<html lang="en">
    <head>
		<title>DBOX Digital Library</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" sizes="221x221" href="../images/icon.png">
		<link rel="apple-touch-icon" sizes="221x221" href="../images/icon.png">

		<!-- jQuery -->
		<!-- Javascript -->
		<script src="js/jquery-3.2.1.min.js"></script>
		
		<!-- Bootstrap -->
		<!-- CSS -->
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Javascript -->
		<script src="js/bootstrap.min.js"></script>

		<!-- Other -->
		<!-- Javascript -->

		<?php
			require_once("action/HeaderAction.php");
			$actionHeader = new HeaderAction();
			$actionHeader->execute();
		?>

    </head>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>                        
				</button>
				<div class="navbar-brand">DBOX Digital Library</div>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav navbar-left">
					<li><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<?php
					// Logged in
					if (empty($_SESSION["user"])) {
					?>
					<li><a href="signup.php"><span class="glyphicon glyphicon-pencil"></span> Sign Up</a></li>
					<li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Log in</a></li>
					<?php
					}
					// Logged out
					else {
						?>
					<li><a href="profile.php"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
					<li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
					
					<?php
					}
					?>	
				</ul>
				</div>
			</div>
		</nav>

	
		<div class="container">
			
		